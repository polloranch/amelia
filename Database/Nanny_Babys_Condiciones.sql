-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: Nanny
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Babys_Condiciones`
--

DROP TABLE IF EXISTS `Babys_Condiciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Babys_Condiciones` (
  `idBabys_Condiciones` int(11) NOT NULL AUTO_INCREMENT,
  `idBaby` int(11) NOT NULL,
  `idCondiciones` int(11) NOT NULL,
  `Tratamiento` varchar(45) DEFAULT NULL,
  `Medicamento` varchar(45) DEFAULT NULL,
  `Tel_Emergencia` varchar(45) DEFAULT NULL,
  `Nombre_Emergencia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idBabys_Condiciones`),
  KEY `fk_Babys_Condiciones_1_idx` (`idBaby`),
  KEY `fk_Babys_Condiciones_2_idx` (`idCondiciones`),
  CONSTRAINT `fk_Babys_Condiciones_1` FOREIGN KEY (`idBaby`) REFERENCES `Babies` (`idBabys`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Babys_Condiciones_2` FOREIGN KEY (`idCondiciones`) REFERENCES `Condiciones` (`idCondiciones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Babys_Condiciones`
--

LOCK TABLES `Babys_Condiciones` WRITE;
/*!40000 ALTER TABLE `Babys_Condiciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `Babys_Condiciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-27 21:17:37
