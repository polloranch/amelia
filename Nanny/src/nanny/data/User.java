/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nanny.data;

/**
 *
 * @author juanpablo
 */
public class User {
    private int idUser;
    private String name;
    private String birthday;
    private String cc;    
    private String residence;
    private String cel;
    private String address;
    private int typeOfUser;
    private String userName;
    private String password;
    
    public User(int idUser, String name, String birthday, String cc, String residence, 
            String cel, String address, int typeOfUser, String userName, String password) {
        this.idUser = idUser;
        this.name = name;
        this.birthday = birthday;
        this.cc = cc;
        this.residence = residence;
        this.cel = cel;
        this.address = address;
        this.typeOfUser = typeOfUser;
        this.userName = userName;
        this.password = password;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getName() {
        return name;
    }

    public String getBirthday() {
        return birthday;
    }

    public String getCc() {
        return cc;
    }

    public String getResidence() {
        return residence;
    }

    public String getCel() {
        return cel;
    }

    public String getAddress() {
        return address;
    }

    public int getTypeOfUser() {
        return typeOfUser;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
