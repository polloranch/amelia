/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nanny.data;

/**
 *
 * @author juanpablo
 */
public class Nanny extends User {
    private int idNanny;
    private String educationlevel;
    private String studies;
    private int dispHours;
    private String initHour;
    private String finalHour;
    private double price;
    private boolean assigned;
    private String speciality;

    public Nanny(int idNanny, String educationlevel, String studies, int dispHours, 
            String initHour, String finalHour, double price, boolean assigned, 
            String speciality, int idUser, String name, String birthday, String cc, 
            String residence, String cel, String address, int typeOfUser, String userName, 
            String password) 
    {
        super(idUser, name, birthday, cc, residence, cel, address, typeOfUser, userName, password);
        this.idNanny = idNanny;
        this.educationlevel = educationlevel;
        this.studies = studies;
        this.dispHours = dispHours;
        this.initHour = initHour;
        this.finalHour = finalHour;
        this.price = price;
        this.assigned = assigned;
        this.speciality = speciality;
    }    

    public int getIdNanny() {
        return idNanny;
    }

    /**
     * @return the educationlevel
     */
    public String getEducationlevel() {
        return educationlevel;
    }

    /**
     * @return the studies
     */
    public String getStudies() {
        return studies;
    }

    /**
     * @return the dispHours
     */
    public int getDispHours() {
        return dispHours;
    }

    /**
     * @return the initHour
     */
    public String getInitHour() {
        return initHour;
    }

    /**
     * @return the finalHour
     */
    public String getFinalHour() {
        return finalHour;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @return the isAssigned
     */
    public boolean isAssigned() {
        return assigned;
    }

    /**
     * @return the speciality
     */
    public String getSpeciality() {
        return speciality;
    }
}
