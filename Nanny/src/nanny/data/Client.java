/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nanny.data;

/**
 *
 * @author juanpablo
 */
public class Client extends User{
    private int idClient;
    private int numKids;
    private String addressKids;
    private String nameEmerContact;    
    private String telEmerContact;
    private String relation;
    private String jobAddress;
    private String jobTel;
    private int idNanny;

    public Client(int idClient, int numKids, String addressKids, String nameEmerContact, 
            String telEmerContact, String relation, String jobAddress, String jobTel, 
            int idNanny, int idUser, String name, String birthday, String cc, 
            String residence, String cel, String address, int typeOfUser, 
            String userName, String password) 
    {
        super(idUser, name, birthday, cc, residence, cel, address, typeOfUser, userName, password);
        this.idClient = idClient;
        this.numKids = numKids;
        this.addressKids = addressKids;
        this.nameEmerContact = nameEmerContact;
        this.telEmerContact = telEmerContact;
        this.relation = relation;
        this.jobAddress = jobAddress;
        this.jobTel = jobTel;
        this.idNanny = idNanny;
    }

    public int getIdClient() {
        return idClient;
    }

    public int getNumKids() {
        return numKids;
    }

    public String getAddressKids() {
        return addressKids;
    }

    public String getNameEmerContact() {
        return nameEmerContact;
    }

    public String getTelEmerContact() {
        return telEmerContact;
    }

    public String getRelation() {
        return relation;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public String getJobTel() {
        return jobTel;
    }

    public int getIdNanny() {
        return idNanny;
    }
}