/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nanny.bussiness_logic;

import java.sql.SQLException;
import java.sql.ResultSet;

import nanny.data.Client;
        
/**
 *
 * @author juanpablo
 */
public class FlowLogic {
    
    public static int continueBtn(String place, String date, int hours, int kids, 
            String relation, String requ, String jobAddr, String jobTel, 
            String nameEm, String telEm, int idUser) throws SQLException, ClassNotFoundException
    {
         if (!"".equals(place) && !"".equals(date) && !"".equals(hours) &&
                 !"".equals(kids) && !"".equals(relation) && !"".equals(requ)
                 && !"".equals(jobAddr) && !"".equals(jobTel) && !"".equals(nameEm)
                 && !"".equals(telEm))
         {
             DatabaseControl dc = new DatabaseControl();
             ResultSet rs;
             String q = "iduser='" + idUser + "'";
             rs = dc.search("USER", q);
             rs.next();
             Client client = new Client(0, kids, place, nameEm, telEm, relation,
                     jobAddr, jobTel, 0, idUser, rs.getString("name"), rs.getString("birthday"),
                     rs.getString("idnumber"), rs.getString("city"), rs.getString("phone"),
                     rs.getString("address"), rs.getInt("user_role"), rs.getString("username"),
                     rs.getString("password"));
             
             dc.mySqlConnection();
             if (dc.insertData("CLIENT", client) == 1)
                 return 0;
             else
                 return 1;
         }
         else
             return 2;
    }
}
