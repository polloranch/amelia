/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nanny.bussiness_logic;

import java.sql.ResultSet;
import java.sql.SQLException;
import nanny.data.User;

/**
 *
 * @author manu
 */
public class NewUser {
    public static String create_user(
            String name,
            String birthday,
            String id,
            String city,
            String phone,
            String address,
            String username,
            String password
    ) throws SQLException, ClassNotFoundException{
        String table = "USER";
        String queryCondition = "name='" + name + "'";
        String response = "";
        ResultSet request;
        int validate = -1;
       
        User newUser = new User(0,name,birthday,id,city,phone,address,1,username, password);
    
        DatabaseControl queryControl = new DatabaseControl();
        queryControl.mySqlConnection();
        request = queryControl.search(table, queryCondition);
      
        
        if (request != null) {    
           response = "El usuario ya existe, por favor ingrese un usuario nuevo.";
        }
        else
        {
            System.out.println("Inicia ingreso de usuario en BD.");
            validate = queryControl.insertData(table, newUser);
        }
        System.out.println(validate);
        if (validate > 0)
            response = "El usuario fue ingresado exitosamente.";
        else
            response = "El usuario no pudo ingresarse al sistema.";
        return response;
    }
    
    public static int login(
            String username,
            String password    
    ) throws SQLException, ClassNotFoundException{
        String table = "USER";
        String queryCondition = "username='" + username + "' AND password='" + password + "' LIMIT 1";
        int response = -1;
        ResultSet request;
            
        DatabaseControl queryControl = new DatabaseControl();
        queryControl.mySqlConnection();
        request = queryControl.search(table, queryCondition);        
        
        while (request.next())
        {
            if (request == null)
            {
                response = -2;
            }
            else if ((!request.getString("username").equals(username)) || (!request.getString("password").equals(password)))
            {
                response = -1;
            }
            else
            {
                response = request.getInt("iduser");
                System.out.println("Id Usuario:" + response);
            }
        }        
        return response;
    }
}

