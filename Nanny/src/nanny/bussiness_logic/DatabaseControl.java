/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nanny.bussiness_logic;

import nanny.data.Nanny;
import nanny.data.User;
import nanny.data.Client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

/**
 *
 * @author juanpablo
 */
public class DatabaseControl {
    private static Connection con;
    private static final String user = "root";
    private static final String password = "root";
    private static final String host = "localhost";
    private static final String port = "3306";
    private static final String DB = "Nanny";
    
    public void mySqlConnection() throws SQLException, ClassNotFoundException
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + DB, user, password);   
        }
        catch (SQLException ex)
        {
            System.err.println("ERROR DE CONEXIÓN: " + ex.toString());
        }
        catch (ClassNotFoundException ex)
        {
            System.err.println("CLASE NO ENCONTRADA: " + ex.toString()); 
        }
    }
    
    public void closeConnection() {
        try {
            con.close();
            System.out.println("Se ha finalizado la conexión con el servidor");
        } catch (SQLException ex) {
            System.err.println("ERROR DE DESCONEXION: " + ex.toString()); 
        }
    }
    
    public int insertData(String table_name, Nanny nanny) 
    {
        String Query = "INSERT INTO " + table_name + " (iduser, studies,"
                + "availability, start_time, date_time, price, specialty,"
                + "assigned) VALUES(";
        Query += "'" + nanny.getIdUser() + "', " 
                + "'" + nanny.getStudies() + "', "
                + "'" + nanny.getDispHours() + "', "
                + "'" + nanny.getInitHour() + "', "
                + "'" + nanny.getFinalHour() + "', "
                + "'" + nanny.getPrice() + "', "
                + "'" + nanny.getSpeciality() + "', "
                + "'" + nanny.isAssigned() + "')";
        return doInsert(Query);
    }
    
    public int insertData(String table_name, User user)
    {
        String Query = "INSERT INTO " + table_name + " (name, birthday, idnumber, "
                + "city, phone, address, user_role, username, password) VALUES(";
        Query += "'" + user.getName() + "', " 
                + "'" + user.getBirthday()+ "', "
                + "'" + user.getCc() + "', "
                + "'" + user.getResidence() + "', "
                + "'" + user.getCel() + "', "
                + "'" + user.getAddress() + "', "
                + "'" + user.getTypeOfUser() + "', "
                + "'" + user.getUserName() + "', "
                + "'" + user.getPassword() + "')";
        return doInsert(Query);
    }
    
    public int insertData(String table_name, Client client) 
    {
        String Query = "INSERT INTO " + table_name + "(iduser, num_kids, "
                + "address_kids, emergency_contact, emergency_phone, realation,"
                + "work_address, work_phone, idnanny) VALUES(";
        Query += "'" + client.getIdUser() + "', " 
                + "'" + client.getNumKids() + "', "
                + "'" + client.getAddressKids() + "', "
                + "'" + client.getNameEmerContact() + "', "
                + "'" + client.getTelEmerContact() + "', "
                + "'" + client.getRelation() + "', "
                + "'" + client.getJobAddress() + "', "
                + "'" + client.getJobTel() + "', "
                + "'" + client.getIdNanny() + "')";
        return doInsert(Query);
    }
    
    public ResultSet search(String table, String req)
    {
        String Query;
        if (!"".equals(req))
            Query = "SELECT * FROM " + table + " WHERE " + req;
        else
            Query = "SELECT * FROM " + table;
        return doSelect(Query);
    }
    
    public int update(String table, String upd, String filter)
    {
        String Query = "UPDATE " + table + " SET " + upd + " WHERE " + filter;
        return doInsert(Query);
    }
    
    public int doInsert(String q)
    {
        try
        {
            Statement st = con.createStatement();
            return st.executeUpdate(q);
        }
        catch (SQLException ex)
        {
            System.err.println(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error con la conexión de la base de datos");
        }
        return 0;
    }
    
    public ResultSet doSelect(String q)
    {
        try
        {
            Statement st = con.createStatement();
            ResultSet resultSet;
            resultSet = st.executeQuery(q);
            return resultSet;
        } 
        catch (SQLException ex) 
        {
            System.out.println(ex.getMessage());
        }
        
        return null; 
    }
}
